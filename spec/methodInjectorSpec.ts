import { Container } from "../src/Container";
import { Entity, StaticEntity } from "../src/Example/Entity";
import { resetContainer } from "./helpers/resetLiveContainer";

describe("An injectable method of an Entity", () => {
  let container: Container
  beforeEach(() => {
    container = resetContainer()
    container.registerValue("greetmsg", "My name is")
  })
  it("should use the orginal argument passed when no matched value is registered", () => {
    let entity = new Entity()
    expect(entity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Heisenberg. Nice to meet you.")
  })
  it("should inject the registered value", () => {
    container.registerValue("myname", "Jeff")
    container.registerValue("meetmsg", "I don't know what you're talking about..")
    let entity = new Entity()
    expect(entity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Jeff. I don't know what you're talking about..")
  })
  it("should inject the registered value but use the oringal arguments if the key is not registered in the container", () => {
    container.registerValue("myname", "Jeff")
    let entity = new Entity()
    expect(entity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Jeff. Nice to meet you.")
  })
})

describe("An injectable method of a static Entity", () => {
  let container: Container
  beforeEach(() => {
    container = resetContainer()
    container.registerValue("greetmsg", "My name is")
  })
  it("should use the orginal argument passed when no matched value is registered", () => {
    expect(StaticEntity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Heisenberg. Nice to meet you.")
  })
  it("should inject the registered value", () => {
    container.registerValue("myname", "Jeff")
    container.registerValue("meetmsg", "I don't know what you're talking about..")
    expect(StaticEntity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Jeff. I don't know what you're talking about..")
  })
  it("should inject the registered value but use the oringal arguments if the key is not registered in the container", () => {
    container.registerValue("myname", "Jeff")
    expect(StaticEntity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."})).toBe("My name is Jeff. Nice to meet you.")
  })
})