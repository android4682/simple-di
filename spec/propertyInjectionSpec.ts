import { Container } from "../src/Container";
import { Entity, StaticEntity } from "../src/Example/Entity"
import { resetContainer } from "./helpers/resetLiveContainer";

describe("An injectable property of an Entity", () => {
  let container: Container
  beforeEach(() => {
    container = resetContainer()
  })
  it("should be empty when no matched value is registered", () => {
    let entity = new Entity()
    expect(entity.greetmsg).toBeUndefined()
  })
  it("should return the registered value", () => {
    container.registerValue("greetmsg", "My name is")
    let entity = new Entity()
    expect(entity.greetmsg).toBe("My name is")
  })
  it("should not be settable", () => {
    container.registerValue("greetmsg", "My name is")
    let entity = new Entity()
    entity.greetmsg = "Fuck you"
    expect(entity.greetmsg).toBe("My name is")
  })
})

describe("An injectable property of a static Entity", () => {
  let container: Container
  beforeEach(() => {
    container = resetContainer()
  })
  it("should be empty when no matched value is registered", () => {
    expect(StaticEntity.greetmsg).toBeUndefined()
  })
  it("should return the registered value", () => {
    container.registerValue("greetmsg", "My name is")
    expect(StaticEntity.greetmsg).toBe("My name is")
  })
  it("should not be settable", () => {
    container.registerValue("greetmsg", "My name is")
    StaticEntity.greetmsg = "Fuck you"
    expect(StaticEntity.greetmsg).toBe("My name is")
  })
})
