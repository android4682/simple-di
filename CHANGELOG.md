# CHANGELOG for 1.0.\*

This changelog references the relevant changes (bug and security fixes) done in 1.0 minor versions.

- Version 1.0.0 development complete (2022/08/23) *(Initial commit)*
- perf: updated .npmignore to ignore unused files in production - [android4682](https://gitlab.com/android4682/)
- chore: added autodev/cicd/pipelines - [android4682](https://gitlab.com/android4682/)
- fix: updated ci yaml - [android4682](https://gitlab.com/android4682/)
- fix: fixed ci image - [android4682](https://gitlab.com/android4682/)
- fix: auto publish on npm as well - [android4682](https://gitlab.com/android4682/)
- test: created tests, also added to pipeline - [android4682](https://gitlab.com/android4682/)
- fix: added clear function for Container - [android4682](https://gitlab.com/android4682/)
- fix: added missing set function for injectable properties - [android4682](https://gitlab.com/android4682/)
- test: Example class is now compatible with test suite - [android4682](https://gitlab.com/android4682/)