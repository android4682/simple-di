import { Entity, StaticEntity } from "./Entity";
import * as Register from "./Register";

// Entry point
function main()
{
  // Run the value registration processing
  // You could run it manually here but for clarity it's moved to it's own file
  Register.run()
  // Create any entity with the @Inject() decorators
  // This could be any class with @Inject() decorators
  let entity = new Entity()
  // Lets greet the user with injected values!
  // If you want to inject a value you do have to pass it and set it to something
  // If it can't find your injected value the original value will be used
  console.log(entity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."}))
  // It even works on static properties and entities
  console.log(StaticEntity.greet({myname: "Heisenberg", meetmsg: "Nice to meet you."}))
}
main()
