import { Inject } from "../Injector";

export class Entity
{

  // You can auto inject by just adding @Inject() as an annotation above a property
  // You can also choose to override/set the injection key on properties
  // Override/setting the injection key doesn't work on methods.
  // @Inject("greetmsg")
  @Inject()
  greetmsg: string;

  // Great thing is, you won't need to set any properties in your constructor
  // in order for this to work!
  public constructor()
  {
  }

  // Or even on a method, but you won't be able to override any injection keys
  // Make sure your first argument is a destructured object with your injection keys
  // It's okay to mix in other keys that doesn't have any injection values
  // 
  // If the injected value is not registered/set the original argument will be used 
  @Inject()
  public greet({myname, meetmsg}: {myname: string, meetmsg?: string}): string
  {
    return `${this.greetmsg} ${myname}. ${meetmsg}`
  }
}

// This even works on static properties and methods.
export class StaticEntity
{
  // Here we will override the greetmsg just as an example
  @Inject("greetmsg")
  static greetmsg: string

  @Inject()
  public static greet({myname, meetmsg}: {myname: string, meetmsg?: string}): string
  {
    return `${this.greetmsg} ${myname}. ${meetmsg}`
  }
}