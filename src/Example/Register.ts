import { LiveContainer } from "../Container";

export function run()
{
  // Run 'LiveContainer.registerValue' with your key and value
  // SimpleDI will look at variable and argument names and replaces these with the value given here
  // If it can't find a registered value it will just pass the whatever was passed originally
  // 
  // You can register any value you want it will pass it exactly how you registered it
  LiveContainer.registerValue("greetmsg", "My name is")
  LiveContainer.registerValue("myname", "Jeff")
}