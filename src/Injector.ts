// import "reflect-metadata";
import { jsObj } from "./Interfaces/ObjectOverride";
import { LiveContainer } from "./Container";


/**
 * @description Injects values from LiveContainer into a property or method
 * @alias Inject The decorator function for (@Inject())
 * @export
 * @param {string?} ivName A inject name/key to override
 * @returns {Function} 
 */
export function Inject(ivName?: string) {
  return (target: jsObj, propertyKey: string, descriptor?: any) => {
    if (descriptor === undefined) {
      const get = function() {
        return LiveContainer.getInjectedValue(ivName || propertyKey);
      };
      const set = function(value: any) {}
      
      Object.defineProperty(target, propertyKey, { get, set });
    } else {
      const originalFn = <Function> descriptor.value;
      descriptor.value = function (...args: any) {
        if (typeof args[0] === "object" && !Array.isArray(args[0])) {
          for (const key in args[0]) {
            if (Object.prototype.hasOwnProperty.call(args[0], key)) {
              const injectValue = LiveContainer.getInjectedValue(key)
              if (injectValue === undefined) continue
              args[0][key] = injectValue
            }
          }
        }
        return originalFn.call(this, ...args)
      };
    }
  }
}
