/**
 * @description Object that holds a injection name (key) and value
 * @interface InjectionValues
 */
interface InjectionValues
{
  name: string,
  value: any
}

/**
 * @description A DI container
 * @see LiveContainer instead of this Container. Unless you know what you're doing.
 * @export
 * @class Container
 */
export class Container
{
  
  /**
   * @description Contains all the injected values of this container
   *
   * @private
   * @type {InjectionValues[]}
   * @memberof Container
   */
  private valInj: InjectionValues[] = []

  /**
   * Creates an instance of Container.
   * @memberof Container
   */
  public constructor()
  {
  }


  /**
   * @description Finds the injected value based on registered name
   *
   * @private
   * @param {string} name The registered name of the injected value
   * @return {InjectionValues | undefined}
   * @memberof Container
   */
  private findInjectedValue(name: string): InjectionValues | undefined
  {
    return this.valInj.find((elem) => (elem.name === name))
  }


  /**
   * @description Adds the injected value to the internal array
   *
   * @private
   * @param {InjectionValues} iv The injected name and value
   * @memberof Container
   */
  private addInjectionValues(iv: InjectionValues): void
  {
    this.valInj.push(iv)
  }

  /**
   * @description Updates the injection value based on the injection key
   * 
   * @private
   * @param {InjectionValues} iv The injected name and value
   * @returns {boolean} True on success False on doesn't exist
   * @memberof Container
   */
  private setInjectionValue(iv: InjectionValues): boolean
  {
    let fiv = this.findInjectedValue(iv.name)
    if (fiv === undefined) return false
    let index: number = this.valInj.indexOf(fiv)
    this.valInj[index].value = iv.value
    return true
  }


  /**
   * @description Get the injected value by name
   * @param {string} name Name of the injected value
   * @returns {any | undefined} The injected value or undefined if it isn't set
   * @memberof Container
   */
  public getInjectedValue(name: string): any | undefined
  {
    return this.findInjectedValue(name)?.value
  }


  /**
   * @description Registers a new value eligible for injection
   * @param {string} name The name (or key if you will) for the inject value
   * @param {*} value The value you want to inject when (@Inject) is called
   * @memberof Container
   */
  public registerValue(name: string, value: any): void
  {
    if (!this.setInjectionValue({name, value})) this.addInjectionValues({name, value})
  }


  /**
   * @description Clears injected values. Mainly used by the test suite.
   * @memberof Container
   */
  public clear(): void
  {
    this.valInj = []
  }
}

export const LiveContainer = new Container()