# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change. Otherwise you might do work only for it to be declined.

Please note we have a code of conduct, please follow it in all your interactions with the project.

*Once you're done reading our PRP and CoC, see [I want to contribute, what can I do?](#i-want-to-contribute-what-can-i-do)*

## Pull Request Process

1. Ensure no dist, test or build example files are commited.
   - *Your commit message must follow the [Conventional Commits][conventional_commits] rules.*
2. Update the `README.md` if necessary.
3. *(Optional)* Add yourself to the Contributors list in the `README.md` and in `package.json`.
   - *Follow the `* **NAME** - [USERNAME](GITLAB/GITHUB LINK)` template in `README.md`.*
4. Update the `CHANGELOG.md` with your changes.
   - *Please remember to follow the [Conventional Commits][conventional_commits] rules here as well.*
   - *Follow the following example template: `feat: allow provided config object to extend other configs - [name](gitlab/github url)`*
5. Commit and create a pull request.
   - *Please follow the given pull request template when creating a pull request.*

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, mental disablity, level of experience,
nationality, personal appearance, race, religion and sexual orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and friendly language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team at [andrew@androiddd.nl][mailto]. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

# I want to contribute, what can I do?

That's great! You can help out with the following:
- [ ] Checking issues (reacting, testing etc.)
- [ ] Bug fixes
- [ ] Test pull requests
- [ ] Clarify (JS) docs
- [ ] Fix grammer and spelling issues

[](#i-want-to-help-out-what-can-i-do)

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
[mailto]: mailto:andrew@androiddd.nl
[conventional_commits]: https://www.conventionalcommits.org/en/v1.0.0/