const fs = require("node:fs")
const package = JSON.parse(fs.readFileSync("./package.json").toString())
package.name = package.name.split("/")[1]
fs.writeFileSync("./package.json", JSON.stringify(package))
process.exit(0)