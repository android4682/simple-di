# Simple DI

Ever just want a simple way to inject your values? Well now you can!

This package is just that. Simple.

- It IS simple.
- It WORKS simple.
- *Don't make your live harder then it has to be.*

This package is made to be a simple dependecy injector, so no over complicated fancy features.
Just injecting values into properties or methods.

It probably doesn't have the fancy (over-)complicated features some other TypeScript dependcy injectors would have. But that's why it's called **Simple** DI.

**NOTE**: When I tried to publish under the name `simpledi` it already seem to exist by someone named [nerdbeere](https://www.npmjs.com/~nerdbeere) but mine is different enough (I think) to co-exist.

If you want to check out his project [click here](https://github.com/fwdop/simpledi).

## Prerequisites

This project requires NodeJS (version 16 or later) and NPM (or Yarn).
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) (or [Yarn](https://yarnpkg.com/)) are really easy to install.
To make sure you have them available on your machine,
try running the following command.

```sh
$ npm -v && node -v
8.6.0
v16.13.0
```

For `yarn`:
```sh
$ yarn -v
1.22.19
```

## Table of contents

- [Simple DI](#simple-di)
  - [Prerequisites](#prerequisites)
  - [Table of contents](#table-of-contents)
  - [Getting Started](#getting-started)
    - [Installation](#installation)
  - [Example Usage](#example-usage)
    - [Built-in example](#built-in-example)
  - [API](#api)
    - [Inject (@Inject)](#inject-inject)
      - [Options](#options)
    - [getInjectedValue](#getinjectedvalue)
      - [Options](#options-1)
    - [LiveContainer.registerValue](#livecontainerregistervalue)
      - [Options](#options-2)
  - [Contributing](#contributing)
  - [Built With](#built-with)
  - [Versioning](#versioning)
  - [Authors](#authors)
    - [Contributors](#contributors)
  - [License](#license)
  - [Special thanks](#special-thanks)

## Getting Started

To get started you just have to follow the [Installation](#installation) process.
Then check out the [Example](#example-usage) and/or the [API](#api) and you should be good to go!

### Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)

To install and set up the library, run:

```sh
$ npm install -S simpledi-decorators
```

Or if you prefer using `yarn`:

```sh
$ yarn add simpledi-decorators
```

## Example Usage

If you clone this repository, you can run the following commands to run the same example. 
You can find the source of these example files in `./src/Example`.

Via `npm`:
```sh
npm install --include=dev
npm run build-example
npm run example
```

Or for `yarn`:
```sh
yarn install
yarn build-example
yarn example
```

### Built-in example

```ts
// Registrer.ts
import { LiveContainer } from "simpledi-decorators";

// No real reason registrating values is here.
// It's just for clarity, you can move this where ever you want.
export function run()
{
  // Run 'LiveContainer.registerValue' with your key and value.
  // SimpleDI will look at variable and argument names
  // and replaces these with the value given here.
  //
  // If it can't find a registered value
  // it will just pass the whatever was passed originally.
  // 
  // You can register any value you want,
  // it will pass it exactly how you registered it.
  //
  // Classes, instancieted classes, strings, functions, numbers, booleans, anything
  LiveContainer.registerValue("greetmsg", "My name is")
  LiveContainer.registerValue("myname", "Jeff")
}
// At this moment you should NOT make your own container
// unless you want to handle the exporting of it and injection handling yourself.
// 
// Support for this is on the way!
```

```ts
// Entity.ts
import { Inject } from "simpledi-decorators";

export class Entity
{

  // You can auto inject by just adding @Inject() as an annotation above a property.
  // You can also choose to override/set the injection key on properties.
  // Override/setting the injection key doesn't work on methods.
  // @Inject("greetmsg")
  // 
  @Inject()
  greetmsg: string;

  // Great thing is, you won't need to set any properties in your constructor
  // in order for this to work!
  public constructor()
  {
  }

  // Or even on a method, but you won't be able to override any injection keys.
  // Make sure your first argument is a destructured object with your injection keys.
  // It's okay to mix in other keys that doesn't have any injection values.
  // 
  // If the injected value is not registered/set the original argument will be used.
  @Inject()
  public printGreet({myname, meetmsg}: {myname: string, meetmsg?: string})
  {
    console.log(`${this.greetmsg} ${myname}. ${meetmsg}`)
  }
}

// This even works on static properties and methods.
export class StaticEntity
{
  // Here we will override the greetmsg just as an example.
  @Inject("greetmsg")
  static greetmsg: string

  @Inject()
  public static printGreet({myname, meetmsg}: {myname: string, meetmsg?: string})
  {
    console.log(`${this.greetmsg} ${myname}. ${meetmsg}`)
  }
}
```

```ts
// Main.ts (Entry Point)
import { Entity, StaticEntity } from "./Entity";
import * as Register from "./Register";

// Entry point
function main()
{
  // Run the value registration processing.
  // You could run it manually here but for clarity it's moved to it's own file.
  Register.run()
  // Create any entity with the @Inject() decorators.
  // This could be any class with @Inject() decorators.
  let entity = new Entity()
  // Lets greet the user with injected values!
  // If you want to inject a value you do have to pass it and set it to something.
  // If it can't find your injected value the original value will be used.
  entity.printGreet({myname: "Heisenberg", meetmsg: "Nice to meet you."})
  // It even works on static properties and entities.
  StaticEntity.printGreet({myname: "Heisenberg", meetmsg: "Nice to meet you."})
}
main()
```

Your result should be:
```
My name is Jeff. Nice to meet you.
My name is Jeff. Nice to meet you.
```

## API

Every method and property should have JSDocs. If not here you go.

### Inject (@Inject)

```ts
@Inject(ivName?: string)
```

Inject a value by `ivName`.

#### Options

| Argument | Type | Default value | Description | Usage |
| --- | --- | --- | --- | --- |
| `ivName` | string? | undefined | Injection name (key) | If set this will override the property/argument name when getting the injected value from `LiveContainer`. |

Example: See `Entry.ts` in [Built-In Example](#built-in-example).

### getInjectedValue

*This also works on `new Container()` but at this moment is NOT recommanded unless you know what you're doing.*

```ts
LiveContainer.getInjectedValue(name: string): any
```

Get the injected value by name/key.

*You probably won't need this since you have [@Inject](#inject-inject) but if you ever need it here you go.*

#### Options

| Argument | Type | Default value | Description | Usage |
| --- | --- | --- | --- | --- |
| `name` | string | undefined | Injection name (key) | The name of the injection value you want. |

Example:

```ts
import { LiveContainer } from "../Container";

LiveContainer.registerValue("name", "somevalue")

let ivValue = LiveContainer.getInjectedValue("name")
console.log(ivValue) // "somevalue"
```

### LiveContainer.registerValue

*This also works on `new Container()` but at this moment is NOT recommanded unless you know what you're doing.*

```ts
LiveContainer.registerValue(name: string, value: any): void
```

Registers a new value eligible for injection.

#### Options

| Argument | Type | Default value | Description | Usage |
| --- | --- | --- | --- | --- |
| `name` | string | undefined | Injection name (key) | The name of the injection value you want. |
| `value` | string | undefined | Injection value | The value you want it to inject. |

Example: See [Built-In Example](#built-in-example).

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

But to summarize it:

1.  Fork it!
2.  Create your feature branch: `git checkout -b my-new-feature`
3.  Add your changes: `git add .`
4.  Commit your changes: `git commit -am 'Add some feature'`
    -  *Your commit message must follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) rules.*
5.  Push to the branch: `git push origin my-new-feature`
6.  Submit a pull request :sunglasses:

## Built With

* Visual Studio Code
* TypeScript

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Andrew de Jong** - *Lead Developer* - [android4682](https://gitlab.com/android4682)

### Contributors

*You could be the first.. <3 :P*

Also see [I want to contribute, what can I do?](https://gitlab.com/android4682/simple-di/-/blob/master/CONTRIBUTING.md#i-want-to-contribute-what-can-i-do).

## License

[GNU Affero General Public License v3.0](https://gitlab.com/android4682/simple-di/-/blob/master/LICENSE) © Andrew de Jong

## Special thanks

* **Andrea Sonny** - For a [`README.md` template](https://gist.github.com/andreasonny83/7670f4b39fe237d52636df3dec49cf3a) - [andreasonny83](https://gist.github.com/andreasonny83)
* **Billie Thompson** - For a [`CONTRIBUTING.md` template](https://gist.github.com/PurpleBooth/b24679402957c63ec426) - [PurpleBooth](https://gist.github.com/PurpleBooth)